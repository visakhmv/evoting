var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();
const bcrypt = require('bcrypt');
var session = require('express-session');

/* GET home page. */
router.get('/', function(req, res, next) {  
  console.log(req.session);
  res.render('login', { error: false });
});

//     var saltRounds = 10;
//     var stmt = db.prepare("INSERT INTO user(`id`,`username`,`password`) VALUES (NULL,?,?);");
//     var hash = bcrypt.hashSync("123456", saltRounds);

router.post('/',function(req, res, next){
  var db = new sqlite3.Database('mydb.db');
  var hash;
  db.get("SELECT * FROM user where username = ?",[req.body.username], function(err, row) {
    if (err) {
      throw err;
    }
    if(row&&bcrypt.compareSync(req.body.password, row.password)){

      req.session.user=1;
      console.log(req.session);
      req.session.save(function(err) { 
        //res.render('login', { error: false });
        res.redirect('/admin/register');
      })
    }else{      
      res.render('login', { error: true, msg:"Invalid username or password" });
    }
  });
});

router.get('/Election.json',function(req, res, next){
  const request = require('request');
  request('http://localhost:3000/Election.json', function (error, response, body) {
    console.error('error:', error); // Print the error if one occurred
    console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    res.send(body); // Print the HTML for the Google homepage.
  });
});

router.get('/logout', function(req, res, next) {  
  req.session.destroy();
  res.redirect('/');
});

router.get('/publish', function(req, res, next) {
  var db = new sqlite3.Database('mydb.db');
  if(req.query.id){
    db.run('UPDATE candidates SET status = 1 WHERE c_id = ?', [req.query.id], (err) => {
      if(err) {
        return console.log(err.message); 
      }
      res.send('Updated');
    });
  }else{
    res.send('Failed');
  }
});

module.exports = router;
