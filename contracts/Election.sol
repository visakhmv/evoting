pragma solidity >=0.5.0;

contract Election {
	//Admin user address
    address public admin;

    //Flag to set beginning of voting
    bool public votingStart;
    
	// Model a Candidate
    struct Candidate {
        uint id;
        string name;
        string symbol;
        uint asmbly;
        uint voteCount;
    }
    // Read/write Candidates
    mapping(uint => Candidate) public candidates;

    // Store Candidates Count
    uint public candidatesCount;

    event votedEvent (
        uint indexed _candidateId
    );

    // Store accounts that have voted
    mapping(address => bool) public voters;

    // Store accounts that have voted
    mapping(uint => bool) public aadhar;

    // Constructor
    constructor () public {
        // admin = msg.sender;
        // votingStart = false;
    }

    function addCandidate (string memory _name, string memory _symb, uint _asmbly) public {
        //Only admin user can add candidates
        // require(msg.sender==admin,"Permission denied!");

        //Can't add candidate after voting is started
        // require(votingStart == false,"Voting already started!");

        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount, _name, _symb, _asmbly, 0);
    }

    function vote (uint _candidateId, uint _aadhar) public {
        // require that they haven't voted before
        require(!voters[msg.sender],"User already voted!");
        require(!aadhar[_aadhar],"User already voted!");

        // require a valid candidate
        require(_candidateId > 0 && _candidateId <= candidatesCount, "Invalid candidate!");

        votingStart = true;

        // record that voter has voted
        voters[msg.sender] = true;
        aadhar[_aadhar] = true;

        // update candidate vote Count
        candidates[_candidateId].voteCount ++;

	    // trigger voted event
        emit votedEvent(_candidateId);
    }
}