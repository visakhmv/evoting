App = {
  web3Provider: null,
  contracts: {},
  account: '0x0',
  hasVoted: false,
  aadhar: window.sessionStorage.user_id,
  assembly: window.sessionStorage.user_assembly,
  username: window.sessionStorage.user_name,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // TODO: refactor conditional
    if (typeof web3 !== 'undefined') {
      // If a web3 instance is already provided by Meta Mask.
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // Specify default instance if no web3 instance provided
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
    }
    return App.initContract();
  },

  initContract: function() {
    $.getJSON("Election.json", function(election) {
      // Instantiate a new truffle contract from the artifact
      App.contracts.Election = TruffleContract(election);
      // Connect provider to interact with contract
      App.contracts.Election.setProvider(App.web3Provider);

      App.listenForEvents();

      return App.render();
    });
  },

  // Listen for events emitted from the contract
  listenForEvents: function() {
    App.contracts.Election.deployed().then(function(instance) {
      instance.votedEvent({}, {
        fromBlock: 'latest',
        toBlock: 'latest'
      }).watch(function(error, event) {
        console.log("event triggered", event)
        // Reload when a new vote is recorded
        App.render(1);
      });
    });
  },

  render: function(data) {
    // if(data!=1)
    //   return;
    var electionInstance;
    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    // Load account data
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        // App.account = web3.eth.accounts[0];
        // alert(window.sessionStorage.user_data);
        App.account = window.sessionStorage.user_key;
        $("#accountAddress").html(App.username +" - "+App.aadhar);
        console.log("Voting account : "+App.account);
      }
    });
    //console.log(web3.eth.accounts);
    // Load contract data
    App.contracts.Election.deployed().then(function(instance) {
      electionInstance = instance;
      //alert("1");
      return electionInstance.candidatesCount();
    }).then(function(candidatesCount) {

      var candidatesSelect = $('#candidatesSelect');
      candidatesSelect.empty();

      for (var i = 1; i <= candidatesCount; i++) {
        //alert(i);
        electionInstance.candidates(i).then(function(candidate) {
          
          var id = candidate[0];
          var name = candidate[1];
          var img = candidate[2];
          var assembly = candidate[3];

          //Enable when duplicate list is rendered
          if(id==1){
            candidatesSelect.empty();
          }
          //Filter candidates by assembly
          if(assembly==App.assembly){
            // Render candidate ballot option
            var candidateOption = "<tr><td>"+ id + "</td><td>" + name + "</td>";
            candidateOption += "<td><img src='http://localhost:8080/img/candidate/"+img+"' width=60/></td></div>"
            candidateOption += "<td><label class='radio-box'>"+
            "<input required type='radio' name='candidate' value='" + id + "' />"+
            "<span class='checkmark'></span></label></td>";
            candidatesSelect.append(candidateOption);
          }
        });
      }
      
      return electionInstance.voters(App.account);
    }).then(function(hasVoted) {
      // Do not allow a user to vote
      if(hasVoted) {
        $("#voteStatus").html("<h3 style='text-align:center'>Your vote has been registered!</h3>");
        $('form').empty();
        $.ajax({
          url: "http://localhost:8080/api/voted",
      
          // The name of the callback parameter, as specified by the YQL service
          jsonp: "callback",
      
          // Tell jQuery we're expecting JSONP
          dataType: "jsonp",
      
          // Tell YQL what we want and that we want JSON
          data: {
              data: App.aadhar,
              format: "json"
          },
      
          // Work with the response
          success: function( response ) {
              console.log('vote logged to server');
          }
        });
      }
      loader.hide();
      content.show();
    }).catch(function(error) {
      console.warn(error);
    });
  },

  castVote: function() {
    var candidateId = $('input:radio[name=candidate]:checked').val();
    App.contracts.Election.deployed().then(function(instance) {
      return instance.vote(candidateId,App.aadhar, { from: App.account, gas:3000000  });
    }).then(function(result) {
      // Wait for votes to update
      $("#content").hide();
      $("#loader").show();
    }).catch(function(err) {
      console.error(err);
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});